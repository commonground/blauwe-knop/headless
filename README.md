# headless

Execute the BK protocol without using the app.

## Installation

Prerequisites:

- [Git](https://git-scm.com/)
- [Golang](https://golang.org/doc/install)
- [Modd](https://github.com/cortesi/modd)

## Execute

Login using registrator

```sh
docker run \
   -e BSN='814859094' \
   -e ORGANIZATION_OIN='01634811392636186840' \
   -e REGISTRATOR_ORGANIZATION_OIN='01634811383832203175' \
   registry.gitlab.com/commonground/blauwe-knop/headless:<tag>
```

```txt
Usage:
  main [OPTIONS]

Application Options:
      --bsn=                          BSN (default: 814859094) [$BSN]
      --organization-oin=             Organization OIN (default: 01634811392636186840) [$ORGANIZATION_OIN]
      --registrator-organization-oin= Registrator Organization OIN (default: 01634811383832203175) [$REGISTRATOR_ORGANIZATION_OIN]
  -o, --online                        Check if endpoints are online [$ONLINE]
  -c, --health-check                  Check if endpoints are working [$HEALTH-CHECK]
  -f, --functional                    Functional test endpoints [$FUNCTIONAL]

Help Options:
  -h, --help                          Show this help message
```

---

`DEPRECATED` Manual login

```sh
go run flow-1/main.go
```

---

## Execute on development environment

1. This repository expects all other BK repositories to be cloned in the same parent-directory.
   Make sure every application is able to run (eg. dependencies have been downloaded etc.)

```sh
ls
```

Expected output:

```sh
app-debt-process/
app-link-process/
blauwe-knop/
debt-register/
debt-request-register/
debt-request-process/
deployment/
headless/
link-register/
login-page/
session-process/
scheme/
session-register/
```

1. As we got all the necessary repositories we navigate to the this repository

```sh
cd headless
```

1. Prepare environment (spin-up Redis + clean database)

```sh
./start.sh
```

1. Start all services

```sh
cd ../blauwe-knop && modd
```

### Execute test

```sh
go run flow-2/cmd/*.go \
   --bsn 814859094 \
   --organization-oin 00000000000000000001 \
   --registrator-organization-oin 00000000000000000001
```
