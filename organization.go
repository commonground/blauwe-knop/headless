package headless

type Organization struct {
	OIN               string
	Name              string
	APIBaseURL        string
	AppLinkProcessUrl string
	SessionProcessUrl string
	RegistratorUrl    string
}
