package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
)

func main() {
	bsnPietVerstraten := "814859094"

	appDebtProcess := "http://localhost:8080/api/v1"
	appLinkProcess := "http://localhost:8089"

	authToken := createAuthToken(appDebtProcess)
	log.Printf("auth token: %s", authToken)

	redirectURL := login(appLinkProcess, authToken, bsnPietVerstraten)
	log.Printf("redirect url: %s", redirectURL)

	sessionToken := exchangeAuthTokenForSessionToken(appDebtProcess, authToken)
	log.Printf("session token: %s", sessionToken)

	schuldenOverzicht := getSchuldenOverzicht(appDebtProcess, sessionToken)
	log.Printf("schulden overzicht: %s", schuldenOverzicht)
}

func getSchuldenOverzicht(baseURL string, sessionToken string) string {
	client := &http.Client{}
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/schuldenoverzicht", baseURL), nil)
	req.Header.Add("Authorization", sessionToken)
	resp, err := client.Do(req)
	if err != nil {
		print(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		print(err)
	}

	return string(body)
}

func exchangeAuthTokenForSessionToken(baseURL string, authToken string) string {
	resp, err := http.Get(fmt.Sprintf("%s/authtoken/exchange?authtoken=%s", baseURL, authToken))
	if err != nil {
		print(err)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		print(err)
	}

	return string(body)
}

func login(baseURL string, authToken string, bsn string) string {
	formData := url.Values{
		"authtoken": {authToken},
		"bsn":       {bsn},
	}
	resp, err := http.PostForm(fmt.Sprintf("%s/auth/login", baseURL), formData)
	if err != nil {
		print(err)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		print(err)
	}

	return string(body)
}

func createAuthToken(baseURL string) string {
	resp, err := http.Get(fmt.Sprintf("%s/authtoken/create", baseURL))
	if err != nil {
		print(err)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		print(err)
	}

	return string(body)
}
