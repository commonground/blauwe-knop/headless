# Use go 1.x based on alpine image.
FROM golang:1.17.3-alpine AS build

ADD . /go/src/headless/
ENV GO111MODULE on
WORKDIR /go/src/headless
RUN go mod download
RUN go build -o dist/bin/headless ./flow-2/cmd/

# Release binary on latest alpine image.
FROM alpine:latest

COPY --from=build /go/src/headless/dist/bin/headless /usr/local/bin/headless

# Add non-priveleged user
RUN adduser -D -u 1001 appuser
USER appuser
CMD ["/usr/local/bin/headless"]
