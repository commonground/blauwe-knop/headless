// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"encoding/json"
	"fmt"
	"net/http"

	"headless"
)

type schemeResponseModel []*struct {
	OIN               string `json:"oin"`
	Name              string `json:"name"`
	APIBaseURL        string `json:"apiBaseUrl"`
	AppLinkProcessUrl string `json:"appLinkProcessUrl"`
	SessionProcessUrl string `json:"sessionProcessUrl"`
	RegistratorUrl    string `json:"registratorUrl"`
}

type SchemeRepository struct {
	baseURL string
}

func NewSchemeRepository(baseURL string) *SchemeRepository {
	return &SchemeRepository{
		baseURL: baseURL,
	}
}

func (s *SchemeRepository) GetOrganizationByOIN(oin string) (*headless.Organization, error) {
	resp, err := http.Get(s.baseURL)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch scheme: %v", err)
	}

	schemeResponse := &schemeResponseModel{}
	err = json.NewDecoder(resp.Body).Decode(schemeResponse)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	organizations := mapSchemeResponseToModels(schemeResponse)

	for _, organization := range organizations {
		if organization.OIN == oin {
			return organization, nil
		}
	}

	return nil, nil
}

func mapSchemeResponseToModels(schemeResponse *schemeResponseModel) []*headless.Organization {
	var result []*headless.Organization

	for _, organization := range *schemeResponse {
		result = append(result, &headless.Organization{
			OIN:               organization.OIN,
			Name:              organization.Name,
			APIBaseURL:        organization.APIBaseURL,
			AppLinkProcessUrl: organization.AppLinkProcessUrl,
			SessionProcessUrl: organization.SessionProcessUrl,
			RegistratorUrl:    organization.RegistratorUrl,
		})
	}

	return result
}
