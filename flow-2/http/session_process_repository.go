// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"bytes"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"
)

type SessionProcessRepository struct {
	baseURL string
}

type startChallengeResponseModel struct {
	Date   int64  `json:"date"`
	Random string `json:"random"`
}
type completeChallengeResponseModel struct {
	SessionToken string `json:"sessionToken"`
}

func NewSessionProcessRepository(baseURL string) *SessionProcessRepository {
	return &SessionProcessRepository{
		baseURL: baseURL,
	}
}

func (s *SessionProcessRepository) Health() error {
	url := fmt.Sprintf("%s/health", s.baseURL)
	resp, err := http.Get(url)
	if err != nil {
		return fmt.Errorf("failed to fetch sessionProcess health: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("app link process offline")
	}
	return nil
}

func (s *SessionProcessRepository) HealthCheck() (*healthcheck.Result, error) {
	url := fmt.Sprintf("%s/health/check", s.baseURL)
	resp, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch sessionProcess health check: %v", err)
	}

	responseBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read sessionProcess health check body: %v", err)
	}
	log.Print(string(responseBody))

	var result healthcheck.Result
	err = json.Unmarshal(responseBody, &result)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal sessionProcess health check json: %v", err)
	}

	return &result, nil
}

func (a *SessionProcessRepository) GetSessionToken(publicKeyPem string, privateKeyPem rsa.PrivateKey) (string, error) {
	startChallengeResponse, err := a.getSessionStartChallenge(publicKeyPem)
	if err != nil {
		return "", err
	}

	signedMessageContent := fmt.Sprintf("%s%d", startChallengeResponse.Random, startChallengeResponse.Date)
	if err != nil {
		return "", err
	}

	signedMessage, err := signMessage(signedMessageContent, privateKeyPem)
	if err != nil {
		return "", err
	}

	sessionToken, err := a.getSessionCompleteChallenge(publicKeyPem, signedMessage)
	if err != nil {
		return "", err
	}

	return sessionToken, nil
}

func (a *SessionProcessRepository) getSessionStartChallenge(dummyPublicKeyPem string) (*startChallengeResponseModel, error) {
	requestBody := struct {
		PublicKeyPEM string `json:"publicKeyPem"`
	}{
		PublicKeyPEM: dummyPublicKeyPem,
	}

	requestBodyAsJSON, err := json.Marshal(requestBody)
	if err != nil {
		return nil, err
	}

	startChallengeRequest, err := http.NewRequest("POST", fmt.Sprintf("%s/sessions/startchallenge", a.baseURL), bytes.NewReader(requestBodyAsJSON))
	if err != nil {
		return nil, err
	}

	client := &http.Client{}
	startChallengeResponse, err := client.Do(startChallengeRequest)
	if err != nil {
		return nil, err
	}

	defer startChallengeResponse.Body.Close()
	startChallengeResponseBody, err := ioutil.ReadAll(startChallengeResponse.Body)
	if err != nil {
		return nil, err
	}
	log.Printf("startChallengeResponse.Body: %s", string(startChallengeResponseBody))

	var response startChallengeResponseModel
	err = json.Unmarshal(startChallengeResponseBody, &response)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

func signMessage(signedMessageContent string, privateKeyPem rsa.PrivateKey) (string, error) {
	messageBytes := bytes.NewBufferString(signedMessageContent)
	hash := sha256.Sum256(messageBytes.Bytes())

	signedMessage, err := rsa.SignPKCS1v15(rand.Reader, &privateKeyPem, crypto.SHA256, hash[:])
	if err != nil {
		return "", err
	}

	signedMessageEncoded := base64.StdEncoding.EncodeToString(signedMessage)

	return signedMessageEncoded, nil
}

func (a *SessionProcessRepository) getSessionCompleteChallenge(dummyPublicKeyPem string, signedChallenge string) (string, error) {
	requestBodyComplete := struct {
		PublicKeyPEM    string `json:"publicKeyPem"`
		SignedChallenge string `json:"signedChallenge"`
	}{
		PublicKeyPEM:    dummyPublicKeyPem,
		SignedChallenge: signedChallenge,
	}

	requestBodyCompleteAsJSON, err := json.Marshal(requestBodyComplete)
	if err != nil {
		return "", err
	}

	completeChallengeRequest, err := http.NewRequest("POST", fmt.Sprintf("%s/sessions/completechallenge", a.baseURL), bytes.NewReader(requestBodyCompleteAsJSON))
	if err != nil {
		return "", err
	}

	client := &http.Client{}
	completeChallengeResponse, err := client.Do(completeChallengeRequest)
	if err != nil {
		return "", err
	}

	defer completeChallengeResponse.Body.Close()
	completeChallengeResponseBody, err := ioutil.ReadAll(completeChallengeResponse.Body)
	if err != nil {
		return "", err
	}
	log.Printf("completeChallengeResponse.Body: %s", string(completeChallengeResponseBody))

	var response completeChallengeResponseModel
	err = json.Unmarshal(completeChallengeResponseBody, &response)
	if err != nil {
		return "", err
	}
	log.Printf("response.SessionToken: %s", response.SessionToken)

	return response.SessionToken, nil
}
