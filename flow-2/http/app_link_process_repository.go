// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"
)

type AppLinkProcessRepository struct {
	baseURL string
}

func NewAppLinkProcessRepository(baseURL string) *AppLinkProcessRepository {
	return &AppLinkProcessRepository{
		baseURL: baseURL,
	}
}

func (s *AppLinkProcessRepository) Health() error {
	url := fmt.Sprintf("%s/health", s.baseURL)
	resp, err := http.Get(url)
	if err != nil {
		return fmt.Errorf("failed to fetch appLinkProcess health: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("app link process offline")
	}
	return nil
}

func (s *AppLinkProcessRepository) HealthCheck() (*healthcheck.Result, error) {
	url := fmt.Sprintf("%s/health/check", s.baseURL)
	resp, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch appLinkProcess health check: %v", err)
	}

	responseBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read appLinkProcess health check body: %v", err)
	}
	log.Print(string(responseBody))

	var result healthcheck.Result
	err = json.Unmarshal(responseBody, &result)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal appLinkProcess health check json: %v", err)
	}

	return &result, nil
}

func (a *AppLinkProcessRepository) GetLinkToken(debtRequestId string, registratorOin string, sessionToken string) (string, error) {
	url := fmt.Sprintf("%s?debtRequestId=%s&oin=%s&sessionToken=%s", a.baseURL, debtRequestId, registratorOin, sessionToken)
	log.Println(url)
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("app link process returns %s", resp.Status)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(body), nil
}
