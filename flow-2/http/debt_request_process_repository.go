package http

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strings"
)

type DebtRequestProcessRepository struct {
	baseURL string
}

func NewDebtRequestProcessRepository(baseURL string) *DebtRequestProcessRepository {
	return &DebtRequestProcessRepository{
		baseURL: baseURL,
	}
}

func (a *DebtRequestProcessRepository) MakeDebtRequest(oins []string, publicKeyPem string, bsn string) (string, error) {
	cookieJar, err := cookiejar.New(nil)
	if err != nil {
		return "", err
	}

	client := &http.Client{
		Jar: cookieJar,
	}

	requestExchangeToken, err := a.debtRequest(client, oins, publicKeyPem)
	if err != nil {
		return "", err
	}

	err = a.triggerCallback(client, bsn)
	if err != nil {
		return "", err
	}

	return requestExchangeToken, nil
}

func (a *DebtRequestProcessRepository) debtRequest(client *http.Client, oins []string, publicKeyPEM string) (string, error) {
	urlEncodedPublicKey := url.QueryEscape(publicKeyPEM)
	req, err := http.NewRequest("GET", fmt.Sprintf("%s/debt-request-process?organizations=%s&publicKey=%s", a.baseURL, strings.Join(oins, ","), urlEncodedPublicKey), nil)
	if err != nil {
		return "", err
	}

	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}

	if resp.StatusCode != http.StatusOK {
		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return "", err
		}

		log.Printf("make debt request response body: %s", body)
		return "", fmt.Errorf("debt request returns %s", resp.Status)
	}

	requestExchangeToken := ""
	for _, cookie := range resp.Cookies() {
		if cookie.Name == "requestExchangeToken" {
			requestExchangeToken = cookie.Value
		}
	}

	return requestExchangeToken, nil
}

func (a *DebtRequestProcessRepository) triggerCallback(client *http.Client, bsn string) error {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s/debt-request-process/callback?bsn=%s", a.baseURL, bsn), nil)
	if err != nil {
		return err
	}

	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}

		log.Printf("make triggerCallback response body: %s", body)
		return fmt.Errorf("triggerCallback returns %s", resp.Status)
	}
	return nil
}

func (a *DebtRequestProcessRepository) GetRequestIDForExchangeToken(requestExchangeToken string) (string, error) {
	resp, err := http.Get(fmt.Sprintf("%s/debt-request-process/exchangetoken?requestExchangeToken=%s", a.baseURL, requestExchangeToken))
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(body), nil
}
