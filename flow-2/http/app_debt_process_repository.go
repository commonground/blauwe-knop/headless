// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"
)

type AppDebtProcessRepository struct {
	baseURL string
}

func NewAppDebtProcessRepository(baseURL string) *AppDebtProcessRepository {
	return &AppDebtProcessRepository{
		baseURL: baseURL,
	}
}

func (s *AppDebtProcessRepository) Health() error {
	url := fmt.Sprintf("%s/health", s.baseURL)
	resp, err := http.Get(url)
	if err != nil {
		return fmt.Errorf("failed to fetch appDebtProcess health: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("app debt process offline")
	}
	return nil
}

func (s *AppDebtProcessRepository) HealthCheck() (*healthcheck.Result, error) {
	url := fmt.Sprintf("%s/health/check", s.baseURL)
	resp, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch appDebtProcess health check: %v", err)
	}

	responseBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read appDebtProcess health check body: %v", err)
	}
	log.Print(string(responseBody))

	var result healthcheck.Result
	err = json.Unmarshal(responseBody, &result)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal appDebtProcess health check json: %v", err)
	}

	return &result, nil
}

func (a *AppDebtProcessRepository) GetSchuldenOverzicht(linkToken string) (string, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", fmt.Sprintf("%s/schuldenoverzicht", a.baseURL), nil)
	if err != nil {
		return "", err
	}

	req.Header.Add("Authorization", linkToken)

	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(body), nil
}
