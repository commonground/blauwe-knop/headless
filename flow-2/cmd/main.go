package main

import (
	"fmt"
	"log"

	"headless"
	flow2 "headless/flow-2"
	http_infra "headless/flow-2/http"
)

type options struct {
	Bsn                        string `long:"bsn" env:"BSN" default:"814859094" description:"BSN"`
	OrganizationOIN            string `long:"organization-oin" env:"ORGANIZATION_OIN" default:"01634811392636186840" description:"Organization OIN"`
	RegistratorOrganizationOIN string `long:"registrator-organization-oin" env:"REGISTRATOR_ORGANIZATION_OIN" default:"01634811383832203175" description:"Registrator Organization OIN"`
	Online                     bool   `long:"online" short:"o" env:"ONLINE" description:"Check if endpoints are online"`
	HealthCheck                bool   `long:"health-check" short:"c" env:"HEALTH-CHECK" description:"Check if endpoints are working"`
	Functional                 bool   `long:"functional" short:"f" env:"FUNCTIONAL" description:"Functional test endpoints"`
}

func main() {

	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	if len(cliOptions.Bsn) < 1 {
		log.Fatalf("please specify a bsn")
	}

	if len(cliOptions.OrganizationOIN) < 1 {
		log.Fatalf("please specify an organization")
	}

	if len(cliOptions.RegistratorOrganizationOIN) < 1 {
		log.Fatalf("please specify an registrator")
	}

	test_all := !cliOptions.Online && !cliOptions.HealthCheck && !cliOptions.Functional

	schemeRepository := http_infra.NewSchemeRepository(
		"https://gitlab.com/commonground/blauwe-knop/scheme/-/raw/master/organizations.json",
	)

	registratorOrganization, err := getOrganizationByOIN(*schemeRepository, cliOptions.RegistratorOrganizationOIN)
	if err != nil {
		log.Fatalf("error occured [getOrganizationByOIN] for registratorOrganization: %v", err)
	}

	log.Println("--- registratorOrganization information ---")
	log.Printf("Name: %s", registratorOrganization.Name)
	log.Printf("OIN: %s", registratorOrganization.OIN)
	log.Printf("RegistratorUrl: %s", registratorOrganization.RegistratorUrl)
	log.Println()

	sourceOrganization, err := getOrganizationByOIN(*schemeRepository, cliOptions.OrganizationOIN)
	if err != nil {
		log.Fatalf("error occured [getOrganizationByOIN] for sourceOrganization: %v", err)
	}
	log.Println("--- sourceOrganization information ---")
	log.Printf("Name: %s", sourceOrganization.Name)
	log.Printf("OIN: %s", sourceOrganization.OIN)
	log.Printf("APIBaseURL: %s", sourceOrganization.APIBaseURL)
	log.Printf("SessionProcessUrl: %s", sourceOrganization.SessionProcessUrl)
	log.Printf("AppLinkProcessUrl: %s", sourceOrganization.AppLinkProcessUrl)
	log.Println()

	debtRequestProcessRepository := http_infra.NewDebtRequestProcessRepository(registratorOrganization.RegistratorUrl)

	appDebtProcessRepository := http_infra.NewAppDebtProcessRepository(sourceOrganization.APIBaseURL)
	sessionProcessRepository := http_infra.NewSessionProcessRepository(sourceOrganization.SessionProcessUrl)
	appLinkProcessRepository := http_infra.NewAppLinkProcessRepository(sourceOrganization.AppLinkProcessUrl)

	headless_usecase := flow2.NewHeadlessUsecase(
		appDebtProcessRepository,
		sessionProcessRepository,
		appLinkProcessRepository,
		debtRequestProcessRepository,
	)

	if cliOptions.Online || test_all {
		log.Println("--- Online: ---")
		headless_usecase.HealthOnlineHeadlessTest()
		log.Println()
	}

	if cliOptions.HealthCheck || test_all {
		log.Println("--- Health check: ---")
		headless_usecase.HealthCheckHeadlessTest()
		log.Println()
	}

	if cliOptions.Functional || test_all {
		log.Println("--- Functional test: ---")
		headless_usecase.FunctionalHeadlessTest(cliOptions.Bsn, *registratorOrganization, *sourceOrganization)
	}
}

func getOrganizationByOIN(schemeRepository http_infra.SchemeRepository, oin string) (*headless.Organization, error) {
	var organization *headless.Organization
	if oin != "00000000000000000001" {
		org, err := schemeRepository.GetOrganizationByOIN(oin)

		if err != nil {
			return nil, err
		}

		if org == nil {
			return nil, fmt.Errorf("organizaton not found")
		}

		organization = org

	} else {
		organization = &headless.Organization{
			OIN:               oin,
			Name:              "demo-org",
			APIBaseURL:        "http://localhost:8080/api/v1",
			SessionProcessUrl: "http://localhost:8087",
			AppLinkProcessUrl: "http://localhost:8089/auth/request-link-token",
			RegistratorUrl:    "http://localhost:8083",
		}
	}
	return organization, nil
}
