package flow2

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"log"
	"strings"

	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"

	"headless"
)

type HealthChecker interface {
	Health() error
	HealthCheck() (*healthcheck.Result, error)
}

type DebtRequestProcessRepository interface {
	MakeDebtRequest(oins []string, publicKeyPEM string, bsn string) (string, error)
	GetRequestIDForExchangeToken(requestExchangeToken string) (string, error)
}

type AppDebtProcessRepository interface {
	GetSchuldenOverzicht(linkToken string) (string, error)
	HealthChecker
}

type SessionProcessRepository interface {
	GetSessionToken(publicKeyPem string, privateKeyPem rsa.PrivateKey) (string, error)
	HealthChecker
}

type AppLinkProcessRepository interface {
	GetLinkToken(debtRequestId string, registratorOin string, sessionToken string) (string, error)
	HealthChecker
}

type HeadlessUsecase struct {
	appDebtProcessRepository     AppDebtProcessRepository
	sessionProcessRepository     SessionProcessRepository
	appLinkProcessRepository     AppLinkProcessRepository
	debtRequestProcessRepository DebtRequestProcessRepository
}

func NewHeadlessUsecase(appDebtProcessRepository AppDebtProcessRepository,
	sessionProcessRepository SessionProcessRepository,
	appLinkProcessRepository AppLinkProcessRepository,
	debtRequestProcessRepository DebtRequestProcessRepository,
) *HeadlessUsecase {
	return &HeadlessUsecase{
		appDebtProcessRepository:     appDebtProcessRepository,
		sessionProcessRepository:     sessionProcessRepository,
		appLinkProcessRepository:     appLinkProcessRepository,
		debtRequestProcessRepository: debtRequestProcessRepository,
	}
}

func (a *HeadlessUsecase) HealthOnlineHeadlessTest() {
	success := true

	err := a.appDebtProcessRepository.Health()
	if err != nil {
		log.Printf("error online checking source organization app debt process repository: %v", err)
		success = false
	} else {
		log.Printf("sourceOrganization.APIBaseURL online")
	}

	err = a.sessionProcessRepository.Health()
	if err != nil {
		log.Printf("error online checking source organization session process repository: %v", err)
		success = false
	} else {
		log.Printf("sourceOrganization.SessionProcessUrl online")
	}

	err = a.appLinkProcessRepository.Health()
	if err != nil {
		log.Printf("error online checking source organization app link process repository: %v", err)
		success = false
	} else {
		log.Printf("sourceOrganization.AppLinkProcessUrl online")
	}

	if !success {
		log.Fatalln("Online check failed")
	}
}

func (a *HeadlessUsecase) HealthCheckHeadlessTest() {
	success := true

	_, err := a.appDebtProcessRepository.HealthCheck()
	if err != nil {
		log.Printf("error health checking source organization app debt process repository: %v", err)
		success = false
	} else {
		log.Printf("sourceOrganization.APIBaseURL online")
	}

	_, err = a.sessionProcessRepository.HealthCheck()
	if err != nil {
		log.Printf("error health checking source organization session process repository: %v", err)
		success = false
	} else {
		log.Printf("sourceOrganization.SessionProcessUrl online")
	}

	_, err = a.appLinkProcessRepository.HealthCheck()
	if err != nil {
		log.Printf("error health checking source organization app link process repository: %v", err)
		success = false
	} else {
		log.Printf("sourceOrganization.AppLinkProcessUrl online")
	}

	if !success {
		log.Fatalln("Health check failed")
	}
}

func (a *HeadlessUsecase) FunctionalHeadlessTest(bsn string, registratorOrganization headless.Organization, sourceOrganization headless.Organization) {
	privateKey, publicKeyPem, err := createKeyPair()
	if err != nil {
		log.Fatalf("error occured [createKeyPair]: %v", err)
	}

	requestExchangeToken, err := a.debtRequestProcessRepository.MakeDebtRequest([]string{sourceOrganization.OIN}, publicKeyPem, bsn)
	if err != nil {
		log.Fatalf("error occured [debtRequestProcessRepository.MakeDebtRequest]: %v", err)
	}
	log.Printf("requestExchangeToken: %s", requestExchangeToken)

	sessionToken, err := a.sessionProcessRepository.GetSessionToken(publicKeyPem, *privateKey)
	if err != nil {
		log.Fatalf("error occured [sessionProcessRepository.GetSessionToken]: %v", err)
	}
	log.Printf("sessionToken: %s", sessionToken)

	debtRequestID, err := a.debtRequestProcessRepository.GetRequestIDForExchangeToken(requestExchangeToken)
	if err != nil {
		log.Fatalf("error occured [debtRequestProcessRepository.GetRequestIDForExchangeToken]: %v", err)
	}
	log.Printf("request id: %s", debtRequestID)

	linkToken, err := a.appLinkProcessRepository.GetLinkToken(debtRequestID, registratorOrganization.OIN, sessionToken)
	if err != nil {
		log.Fatalf("error occured [appLinkProcessRepository.GetLinkToken]: %v", err)
	}
	log.Printf("link token: %s", linkToken)

	schuldenOverzicht, err := a.appDebtProcessRepository.GetSchuldenOverzicht(linkToken)
	if err != nil {
		log.Fatalf("error occured [appDebtProcessRepository.GetSchuldenOverzicht]: %v", err)
	}
	log.Printf("schulden overzicht: %s", schuldenOverzicht)

	if !strings.Contains(schuldenOverzicht, sourceOrganization.Name) {
		log.Fatalln("Functional test failed!")
	}
}

func createKeyPair() (*rsa.PrivateKey, string, error) {
	key, err := rsa.GenerateKey(rand.Reader, 1024)
	if err != nil {
		return nil, "", err
	}

	pub := key.Public()

	pubPEM := pem.EncodeToMemory(
		&pem.Block{
			Type:  "RSA PUBLIC KEY",
			Bytes: x509.MarshalPKCS1PublicKey(pub.(*rsa.PublicKey)),
		},
	)

	return key, string(pubPEM), nil
}
